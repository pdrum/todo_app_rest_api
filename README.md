# REST API docs
In order to view REST API docs, you should have the npm mpdule `http-server` installed.
You can install it by running the command:
```bash
 npm install http-server -g
```

Once you have `http-server` installed, cd into the `docs` directory of the 
project and run the command:
```bash
http-server -p 9090
```
Open [this link](http://localhost:9090) in your browser and the docs should be there.

# Running the server
Nothing implemented yet.
