CREATE TABLE PERSON (
  ID            BIGSERIAL PRIMARY KEY,
  USERNAME      VARCHAR(250),
  PASSWORD_HASH VARCHAR(250)
);

CREATE TABLE CATEGORY (
  ID       BIGSERIAL PRIMARY KEY,
  OWNER_ID BIGINT REFERENCES PERSON (ID) ON DELETE CASCADE,
  TITLE    VARCHAR(250)
);

CREATE TABLE TODO_ITEM (
  ID          BIGSERIAL PRIMARY KEY,
  OWNER_ID    BIGINT REFERENCES PERSON (ID) ON DELETE CASCADE,
  CATEGORY_ID BIGINT REFERENCES CATEGORY (ID) ON DELETE CASCADE,
  TITLE       VARCHAR(250)
);

