package ir.tts.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * Created by pdrum on 3/8/17.
 */
@Path("/user/login")
public class LoginService extends Service {

    public LoginService() {
        super(LoginService.class);
    }

    @GET
    public String hi() {
        return "hi";
    }

}
