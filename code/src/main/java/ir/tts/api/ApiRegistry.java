package ir.tts.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by pdrum on 3/9/17.
 */
public class ApiRegistry implements Iterable<Class<?>> {

    private List<Class<?>> classes;
    private static ApiRegistry instance;

    private ApiRegistry() {
        this.classes = new ArrayList<>();
    }

    public static ApiRegistry getInstance() {
        if(instance == null) {
            synchronized (ApiRegistry.class) {
                if(instance == null)
                    instance = new ApiRegistry();
            }
        }
        return instance;
    }

    void addClass(Class cls) {
        classes.add(cls);
    }

    @Override
    public Iterator<Class<?>> iterator() {
        return classes.iterator();
    }
}
