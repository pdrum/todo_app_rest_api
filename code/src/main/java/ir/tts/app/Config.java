package ir.tts.app;


import io.dropwizard.Configuration;

/**
 * Created by pdrum on 3/8/17.
 */
public class Config extends Configuration {

    private String nothing;

    public String getNothing() {
        return nothing;
    }

    public void setNothing(String nothing) {
        this.nothing = nothing;
    }
}
