package ir.tts.app;

import io.dropwizard.Application;
import io.dropwizard.jersey.setup.JerseyEnvironment;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import ir.tts.api.ApiRegistry;

/**
 * Created by pdrum on 3/8/17.
 */
public class App extends Application<Config> {

    public static void main(String[] args) throws Exception {
        new App().run(args);
    }

    @Override
    public String getName() {
        return "todo";
    }

    @Override
    public void initialize(Bootstrap<Config> bootstrap) {
        super.initialize(bootstrap);
    }

    public void run(Config config, Environment environment) throws Exception {
        registerWebServices(environment.jersey());
    }

    private void registerWebServices(final JerseyEnvironment jersey) {
        ApiRegistry
                .getInstance()
                .forEach(cls -> jersey.register(cls));
    }

}
